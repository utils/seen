#!/bin/sh
#    Seen, a simple static blog generator
#    Copyright (C) 2021,2022 Ferass 'Vitali64' EL HAFIDI <vitali64pmemail@protonmail.com>
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    Please see the LICENSE file for more details.
usage() {
	echo "Usage     : ./seen [OPTIONS]"
	echo "--help    : Print this help message"
	echo "--clear   : Remove generated blog"
	echo "--license : Show license"
}
die() {
	echo "-> [$(date +%R)] (!) ERROR: Unexpected error, please report : $1 (!)"
	exit 1
}
echo "Seen, a simple static blog generator" 
if [ "$1" = "--clear" ]; then
	echo "-> [$(date +%R)] Deleting generated blog ..."
	rm -Rf www/* || die "Cannot delete generated blog!"
	exit 0
elif [ "$1" = "--help" ]; then
	usage
	exit 0
elif [ "$1" = "--license" ]; then
	echo "Copyright (C) 2021 Vitali64 <vitali64pmemail@protonmail.com>"
	echo
	cat "./LICENSE"
	exit 0
elif [ "$1" = "" ]; then
	echo
else
	echo "-> [$(date +%R)] (!) ERROR : Invalid option, cannot continue. (!)"
	usage
	exit 1
fi
mkdir -p "www" || die "Cannot create www/ directory: mkdir -p returns an error!" # Create the www/ folder if removed
# ---HTML---
cat "templates/header.html" > www/index.html || die "Cannot insert header into index.html!" # Erase www/index.html and insert the header
cat "templates/indextext.html" >> www/index.html || die "Cannot insert indextext into index.html!" # insert indextext
articles="$(find articles/*.md|sed -e 's@.md@@g' -e 's@articles/@@g')" || die "Unknown error" # Detect articles
# Set the defaults
name="(!) no name (!)"
desc="(!) no description (!)"
date="1st January 1970"
for line in ${articles} # In the 'for' loop
do
	. "articles/${line}.cfg" || die "Cannot read ${line}.cfg file!" # Override the defaults
	mkdir "www/articles" -p
	cat "templates/header.html" > "www/articles/${line}.html" || die "Cannot insert header into ${line}.html!" # Erase <article>.html and insert the header
	markdown "articles/${line}.md" >> "www/articles/${line}.html" || hoedown "articles/${line}.md" >> "www/articles/${line}.html" || die "Cannot convert ${line}.md to html!" # Convert the markdown text to html
	cat "templates/footer.html" >> "www/articles/${line}.html" || die "Cannot insert footer into ${line}.html!" # Insert the footer into the article page
	cp -r templates/*.css www/. # Add css files if present
	echo "-> [$(date +%R)] Adding ${line} entry to index.html ..." # Add an entry in index.html
	sed -e "s@path-of-article@articles/${line}@" -e "s@name-of-article@${name}@" -e "s@date-of-article@${date}@" -e "s@description-of-article@${desc}@" \
		"templates/article.html">> "www/index.html" || die "Cannot add entry to index.html!"
done
cat "templates/footer.html" >> "www/index.html" # Insert the footer into the index.html
# ---RSS---
# Set the defaults
rssEnabled="n"
rssBlogTitle="blog"
rssBlogDescription="It's a blog"
. "./rss.cfg" # Override the defaults
if [ "${rssEnabled}" = "n" ]; then
	echo "-> [$(date +%R)] RSS is disabled, skipping..."
	exit 0
fi
# Header of the RSS XML file
printf '%s\n' '<rss version="2.0">' > "www/rss.xml"
printf '%s\n' '<channel>' >> "www/rss.xml"
printf '%s\n' "<title>${rssBlogTitle}</title>" >> "www/rss.xml"
printf '%s\n' "<link>BLANK</link>" >> "www/rss.xml"
printf '%s\n' "<description>$rssBlogDescription</description>" >> "www/rss.xml"
for line in ${articles} # Items
do
	echo "-> [$(date +%R)] Inserting XML ${line} item ..."
	. "articles/${line}.cfg"
	printf '%s\n' '<item>' >> "www/rss.xml"
	printf '%s\n' "<title>${name}</title>" >> "www/rss.xml"
	printf '%s\n' "<description>$(cat articles/${line}.md)</description>" >> "www/rss.xml"
	printf '%s\n' '</item>' >> "www/rss.xml"
done
# Footer of the RSS XML file
printf '%s\n' '</channel>' >> "www/rss.xml"
printf '%s\n' '</rss>' >> "www/rss.xml"
